#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>
#include "md5.h"

const int PASS_LEN=20;        // Maximum any password will be
const int HASH_LEN=33;        // Length of MD5 hash strings

// Given a hash and a plaintext guess, return 1 if
// the hash of the guess matches the given hash.
// That is, return 1 if the guess is correct.
int tryguess(char *hash, char *guess)
{
    // Hash the guess using MD5
    char *hashguess = md5(guess, strlen(guess));
    // Compare the two hashes
    if (strcmp(hash, hashguess) == 0)
    {
        free(hashguess);
        return 1;
    }
    else
    {
        free(hashguess);
        return 0;
    }
    // Free any malloc'd memory

    return 0;
}

// Read in the dictionary file and return the array of strings
// and store the length of the array in size.
// This function is responsible for opening the dictionary file,
// reading from it, building the data structure, and closing the
// file.
char **read_dictionary(char *filename, int *size)
{
    // Create structure to get info from our dictionary file
    struct stat info;
    int dfsize;
    // Checks if we could get info from dictionary file
    int ret = stat(filename, &info);
    if ( ret == -1 )
    {
        printf("ERROR: Could not obtain size of %s", filename);
        exit(1);
    }
    else
    {
        // Returns dictionary file size
        dfsize = info.st_size;
    }
    // Allocates memory to store dictionary file
    char *dsource = malloc(dfsize);
    // Opens dictionary file for reading
    FILE *df;
    df = fopen(filename, "r");
    // Checks if we can open dictionary file for reading
    if(!df)
    {
        printf("ERROR: Could not open %s for reading", filename);
        exit(2);
    }
    else
    {
        // Reads dictionary file into dsource
        fread( dsource, sizeof(char), dfsize, df);
    }
    int wcount = 0;
    // replaces newline characters in the dictionary file array "dsource" with null characters
    // and counts the number of words in dsource
    for(int i = 0; i < dfsize; i++)
    {
        if( dsource[i] == '\n' )
        {
            dsource[i] = '\0';
            wcount++;
        }
    }
    // allocates memory for the dictionary array
    char **dictionary = malloc( wcount * sizeof(char *) );
    // fills dictionary array with the strings found in the dictionary file
    dictionary[0] = &dsource[0];
    int j = 1;
    for ( int i = 0; i < dfsize - 1; i++)
    {
        if( dsource[i] == '\0')
        {
            dictionary[j] = &dsource[i+1];
            j++;
        }
    }
    *size = wcount;
    return dictionary;
}


int main(int argc, char *argv[])
{
    if (argc < 3) 
    {
        printf("Usage: %s hash_file dict_file\n", argv[0]);
        exit(1);
    }

    // Read the dictionary file into an array of strings.
    int dlen;
    char **dict = read_dictionary(argv[2], &dlen);

    // Open the hash file for reading.
    FILE *hf;
    
    hf = fopen(argv[1], "r");
    
    char hline[HASH_LEN];
    // For each hash, try every entry in the dictionary.
    while (fgets(hline, HASH_LEN, hf) != NULL)
    {
        for (int j = 0; j < HASH_LEN; j++)
        {
            if (hline[j] == '\n')
            {
                hline[j] = '\0';
            }
        }
        for(int i = 0; i < dlen; i++)
        {
            if ( tryguess(hline, dict[i]) == 1 )
            {
                printf("Password: %s Hash: %s\n", dict[i], hline ); // Print the matching dictionary entry.
                break;
            }
            
        }
    }
    // Need two nested loops.
}
